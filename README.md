# Website for Jeff's Xonotic Servers

- Resurrection
- Vehicle Warfare

**Website: [https://jeffs.eu/](https://jeffs.eu/)**

## Development

```bash
yarn
```

Run this after each style change:

```bash
yarn run lessc style.less public/static/style.css
```
